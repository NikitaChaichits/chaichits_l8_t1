package b.com.chaichits_l8_t1.customView

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.CheckBox
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import b.com.chaichits_l8_t1.R

class CustomCheckbox (context: Context?, attrs: AttributeSet?) : ConstraintLayout(context, attrs) {

    init{
        inflate(context, R.layout.custom_checkbox, this)

        val textView: TextView = findViewById(R.id.tvQuestion)
        val firstCheckBox: CheckBox = findViewById(R.id.checkBox1)
        val secondCheckBox: CheckBox = findViewById(R.id.checkBox2)

        val attributes = context!!.obtainStyledAttributes(attrs, R.styleable.CustomCheckbox)
        textView.text = attributes.getString(R.styleable.CustomCheckbox_setText)
        firstCheckBox.text = attributes.getString(R.styleable.CustomCheckbox_setCheckbox1)
        secondCheckBox.text = attributes.getString(R.styleable.CustomCheckbox_setCheckbox2)
        attributes.recycle()
    }

    fun setText(text: String) {
        val textView: TextView = findViewById(R.id.tvQuestion)
        textView.text = text
    }

    fun setFirstCheckBox(text: String) {
        val firstCheckBox: CheckBox = findViewById(R.id.checkBox1)
        firstCheckBox.text = text
    }

    fun setSecondCheckBox(text: String) {
        val secondCheckBox: CheckBox = findViewById(R.id.checkBox2)
        secondCheckBox.text = text
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        val secondCheckBox: CheckBox = findViewById(R.id.checkBox2)
        if (secondCheckBox.text == "") {
            secondCheckBox.visibility = View.GONE
        }
        super.onLayout(changed, left, top, right, bottom)


    }

}