package b.com.chaichits_l8_t1.customView

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import b.com.chaichits_l8_t1.R

import java.util.Calendar
import kotlin.math.min
import kotlin.math.roundToInt
import android.view.animation.AnimationUtils.loadAnimation
import android.view.animation.Animation

import android.view.animation.AnimationUtils


class CustomClock : View {


    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        val radius = 30f
        val mPaintRect = Paint()
        mPaintRect.style = Paint.Style.FILL

        val calendar = Calendar.getInstance()
        val hours = calendar.get(Calendar.HOUR_OF_DAY).toFloat()
        val minutes = calendar.get(Calendar.MINUTE).toFloat()
        val seconds = calendar.get(Calendar.SECOND).toFloat()
        val hourHeight = (height / 24) * hours * 0.845f
        val minutesHeight = (height / 60) * minutes * 0.845f
        val secondsHeight = (width / 60) * seconds * 0.845f
        val yMinutesHeight = height - minutesHeight - height/7

        mPaintRect.color = ContextCompat.getColor(context, R.color.colorHours)
        canvas.drawRoundRect((width / 7).toFloat(), height - hourHeight - radius,
                (width / 7 * 2).toFloat(), height - hourHeight + radius, radius, radius, mPaintRect)
        canvas.drawRect((width / 7).toFloat(), height - hourHeight, (width / 7 * 2).toFloat(), height.toFloat(), mPaintRect)

        mPaintRect.color = ContextCompat.getColor(context, R.color.colorMinutes)
        canvas.drawRect((width / 7 * 3).toFloat(), height - minutesHeight, (width / 7 * 4).toFloat(), height.toFloat(), mPaintRect)
        canvas.drawRoundRect((width / 7 * 3).toFloat(), height - minutesHeight - radius,
                (width / 7 * 4).toFloat(), height - minutesHeight + radius, radius, radius, mPaintRect)

        mPaintRect.color = ContextCompat.getColor(context, R.color.colorSeconds)
        canvas.drawRect((width / 7 * 5).toFloat(), height - secondsHeight, (width / 7 * 6).toFloat(), height.toFloat(), mPaintRect)
        canvas.drawRoundRect((width / 7 * 5).toFloat(), height - secondsHeight - radius,
                (width / 7 * 6).toFloat(), height - secondsHeight + radius, radius, radius, mPaintRect)

        paintBackgroundRect(canvas, mPaintRect)
        paintText(canvas, hours, minutes, seconds)

        invalidate()
    }

    private fun paintBackgroundRect(canvas: Canvas, mPaintRect: Paint) {
        mPaintRect.color = ContextCompat.getColor(context, R.color.colorHoursAlfa)
        canvas.drawRect((width / 7).toFloat(), (height / 7).toFloat(),
                (width / 7 * 2).toFloat(), height.toFloat(), mPaintRect)

        mPaintRect.color = ContextCompat.getColor(context, R.color.colorMinutesAlfa)
        canvas.drawRect((width / 7 * 3).toFloat(), (height / 7).toFloat(),
                (width / 7 * 4).toFloat(), height.toFloat(), mPaintRect)

        mPaintRect.color = ContextCompat.getColor(context, R.color.colorSecondsAlfa)
        canvas.drawRect((width / 7 * 5).toFloat(), (height / 7).toFloat(),
                (width / 7 * 6).toFloat(), height.toFloat(), mPaintRect)
    }

    private fun paintText(canvas: Canvas, hours: Float, minutes: Float, seconds: Float) {
        val mPaintText = Paint()
        mPaintText.isAntiAlias = true
        mPaintText.style = Paint.Style.FILL_AND_STROKE
        mPaintText.strokeWidth = 3.0f
        mPaintText.color = Color.BLACK
        mPaintText.textSize = 35f

        canvas.drawText("HOURS", (width/7*1.2).toFloat(), (height / 14).toFloat(), mPaintText)
        canvas.drawText("MINUTES", (width/7*3).toFloat(), (height / 14).toFloat(), mPaintText)
        canvas.drawText("SECONDS", (width/7*5).toFloat(), (height / 14).toFloat(), mPaintText)
        canvas.drawText(hours.roundToInt().toString(), (width/28*5.5).toFloat(), (height - 100).toFloat(), mPaintText)
        canvas.drawText(minutes.roundToInt().toString(), (width/28*13.75).toFloat(), (height - 100).toFloat(), mPaintText)
        canvas.drawText(seconds.roundToInt().toString(), (width/28*21.75).toFloat(), (height - 100).toFloat(), mPaintText)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val mSize = min(widthMeasureSpec, heightMeasureSpec)
        setMeasuredDimension(mSize, mSize)
    }
}
