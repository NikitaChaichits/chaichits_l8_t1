package b.com.chaichits_l8_t1.ui

import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity
import b.com.chaichits_l8_t1.customView.CustomCheckbox
import b.com.chaichits_l8_t1.R

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        val customCheckbox : CustomCheckbox = findViewById(R.id.custom_checkbox1)
        customCheckbox.setText("Are you kidding at me?")
        customCheckbox.setFirstCheckBox("Yes")
        customCheckbox.setSecondCheckBox("No")

        val customCheckbox2 : CustomCheckbox = findViewById(R.id.custom_checkbox2)
        customCheckbox2.setText("Are you kidding at me?Are you kidding at me?Are you kidding at me?")
        customCheckbox2.setFirstCheckBox("VERY LONG TEXT")
        //        customCheckbox2.setSecondCheckBox("VERY LONG TEXTVERY LONG TEXT");

    }
}
