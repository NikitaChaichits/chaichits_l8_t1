package b.com.chaichits_l8_t1.animation

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.View
import b.com.chaichits_l8_t1.R

class FishAnimation(context: Context?, attrs: AttributeSet?) : View(context, attrs) {

    private lateinit var fish: Array<Fish>

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        fish = Array(10, {
            Fish(right - left, bottom - top,
                    context.getDrawable(R.drawable.fish),
                    resources.getDimension(R.dimen.size),
                    resources.getDimension(R.dimen.speed))
        })
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        fish.forEach {
            it.update()
            it.draw(canvas)
        }
        postInvalidateOnAnimation()
    }
}